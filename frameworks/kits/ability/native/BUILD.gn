# Copyright (c) 2021 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//build/ohos.gni")
SUBSYSTEM_DIR = "//foundation/aafwk/standard/frameworks/kits/ability/native"
INNERKITS_PATH = "//foundation/aafwk/standard/interfaces/innerkits"

config("ability_config") {
  visibility = [ ":*" ]
  include_dirs = [
    "${INNERKITS_PATH}/base/include",
    "//utils/native/base/include",
    "${SUBSYSTEM_DIR}/include",
    "//foundation/aafwk/standard/frameworks/kits/appkit/native/app/include",
    "//foundation/aafwk/standard/interfaces/innerkits/app_manager/include/appmgr",
    "${INNERKITS_PATH}/want/include/ohos/aafwk/content",
    "${INNERKITS_PATH}/ability_manager/include",
    "//foundation/aafwk/standard/services/abilitymgr/include",
    "//foundation/appexecfwk/standard/common/log/include",
    "//foundation/appexecfwk/standard/interfaces/innerkits/appexecfwk_core/include/bundlemgr",
    "//foundation/appexecfwk/standard/interfaces/innerkits/appexecfwk_base/include/",
    "//foundation/aafwk/standard/interfaces/innerkits/form_manager/include",
    "//foundation/aafwk/standard/frameworks/kits/appkit/native/ability_runtime/app",
    "//foundation/aafwk/standard/frameworks/kits/appkit/native/app/include",
    "//foundation/aafwk/standard/frameworks/kits/fmskit/native/include",
    "//foundation/aafwk/standard/frameworks/kits/app/native/app/include",
    "//foundation/aafwk/standard/services/common/include",
    "//foundation/distributedschedule/dmsfwk/services/dtbschedmgr/include",
    "//base/global/resmgr_standard/interfaces/innerkits/include",
    "//foundation/aafwk/standard/interfaces/kits/napi/aafwk/featureAbility",
    "//foundation/ace/napi/interfaces/kits",
    "//third_party/node/src",
    "//foundation/communication/ipc/interfaces/innerkits/ipc_core/include",
    "//foundation/aafwk/standard/frameworks/kits/ability/native/include/continuation/remote_register_service",
    "//foundation/aafwk/standard/frameworks/kits/ability/native/include/continuation/distributed",
    "//foundation/aafwk/standard/frameworks/kits/ability/native/include/distributed_ability_runtime",
    "//foundation/aafwk/standard/interfaces/innerkits/ability_manager/include/continuation",
    "//foundation/aafwk/standard/frameworks/kits/ability/native/include/continuation/kits",
    "//base/security/permission/interfaces/innerkits/permission_standard/permissionsdk/main/cpp/include",
    "//foundation/aafwk/standard/common/task_dispatcher/include/dispatcher",
    "//foundation/aafwk/standard/common/task_dispatcher/include/task",
    "//foundation/aafwk/standard/common/task_dispatcher/include/threading",
    "//foundation/aafwk/standard/frameworks/kits/appkit/native/app/include/task",
    "//foundation/aafwk/standard/interfaces/kits/napi/aafwk/inner/napi_common",
  ]

  cflags = []
  if (target_cpu == "arm") {
    cflags += [ "-DBINDER_IPC_32BIT" ]
  }
  defines = [
    "APP_LOG_TAG = \"Ability\"",
    "LOG_DOMAIN = 0xD002200",
  ]
}

config("ability_public_config") {
  visibility = [ ":*" ]
  include_dirs = [
    "${INNERKITS_PATH}/base/include",
    "//foundation/aafwk/standard/frameworks/kits/appkit/native",
    "//foundation/aafwk/standard/frameworks/kits/appkit/native/ability_runtime/app",
    "//foundation/aafwk/standard/frameworks/kits/appkit/native/app/include",
    "//foundation/aafwk/standard/frameworks/kits/appkit/native/ability_runtime",
    "${SUBSYSTEM_DIR}/include",
    "//third_party/libuv/include",
    "//foundation/aafwk/standard/frameworks/kits/appkit/native/ability_runtime/context",
    "//foundation/aafwk/standard/interfaces/innerkits/base/include/ohos/aafwk/base",
    "//foundation/aafwk/standard/frameworks/kits/ability/native/include",
    "//foundation/aafwk/standard/frameworks/kits/ability/native/include/continuation/remote_register_service",
    "//foundation/aafwk/standard/frameworks/kits/ability/native/include/continuation/distributed",
    "//foundation/aafwk/standard/frameworks/kits/ability/native/include/distributed_ability_runtime",
    "//foundation/aafwk/standard/interfaces/innerkits/ability_manager/include/continuation",
    "//base/global/resmgr_standard/interfaces/innerkits/include",
    "//foundation/aafwk/standard/frameworks/kits/ability/native/include/continuation/kits",
    "//foundation/aafwk/standard/frameworks/kits/fmskit/native/include",
    "//foundation/aafwk/standard/interfaces/innerkits/form_manager/include",
    "//foundation/windowmanager/interfaces/innerkits/wm",
    "//foundation/windowmanager/interfaces/kits/napi/window_runtime/window_stage_napi",
    "//third_party/jsoncpp/include",
    "//third_party/json/include",
  ]
}

ohos_shared_library("static_subscriber_ipc") {
  include_dirs = [
    "${SUBSYSTEM_DIR}/include",
    "//base/notification/ces_standard/interfaces/innerkits/native/include",
    "//foundation/aafwk/standard/interfaces/innerkits/want/include/ohos/aafwk/content/",
  ]

  sources = [
    "${SUBSYSTEM_DIR}/src/static_subscriber_proxy.cpp",
    "${SUBSYSTEM_DIR}/src/static_subscriber_stub.cpp",
  ]

  deps = [
    "//foundation/distributedschedule/samgr/interfaces/innerkits/samgr_proxy:samgr_proxy",
    "//utils/native/base:utils",
  ]

  external_deps = [
    "ability_base:want",
    "ces_standard:cesfwk_innerkits",
    "ipc:ipc_core",
  ]

  subsystem_name = "aafwk"
  part_name = "ability_runtime"
}

ohos_shared_library("abilitykit_native") {
  include_dirs = [
    "//foundation/distributeddatamgr/appdatamgr/frameworks/jskitsimpl/napi_rdb/include",
    "//foundation/distributeddatamgr/appdatamgr/frameworks/jskitsimpl/common/include",
    "//foundation/distributeddatamgr/appdatamgr/frameworks/jskitsimpl/napi_dataability/include",
    "//foundation/distributeddatamgr/appdatamgr/frameworks/jskitsimpl/napi_resultset/include",
    "//foundation/distributeddatamgr/objectstore/interfaces/innerkits/",
    "//base/global/i18n_standard/frameworks/intl/include",
  ]

  sources = [
    "${INNERKITS_PATH}/app_manager/src/appmgr/process_info.cpp",
    "${SUBSYSTEM_DIR}/src/ability.cpp",
    "${SUBSYSTEM_DIR}/src/ability_context.cpp",
    "${SUBSYSTEM_DIR}/src/ability_handler.cpp",
    "${SUBSYSTEM_DIR}/src/ability_impl.cpp",
    "${SUBSYSTEM_DIR}/src/ability_impl_factory.cpp",
    "${SUBSYSTEM_DIR}/src/ability_lifecycle.cpp",
    "${SUBSYSTEM_DIR}/src/ability_lifecycle_executor.cpp",
    "${SUBSYSTEM_DIR}/src/ability_loader.cpp",
    "${SUBSYSTEM_DIR}/src/ability_local_record.cpp",
    "${SUBSYSTEM_DIR}/src/ability_post_event_timeout.cpp",
    "${SUBSYSTEM_DIR}/src/ability_process.cpp",
    "${SUBSYSTEM_DIR}/src/ability_thread.cpp",
    "${SUBSYSTEM_DIR}/src/ability_window.cpp",
    "${SUBSYSTEM_DIR}/src/data_ability_helper.cpp",
    "${SUBSYSTEM_DIR}/src/data_ability_impl.cpp",
    "${SUBSYSTEM_DIR}/src/data_ability_operation.cpp",
    "${SUBSYSTEM_DIR}/src/data_ability_operation_builder.cpp",
    "${SUBSYSTEM_DIR}/src/data_ability_result.cpp",
    "${SUBSYSTEM_DIR}/src/data_uri_utils.cpp",
    "${SUBSYSTEM_DIR}/src/datashare_connection.cpp",
    "${SUBSYSTEM_DIR}/src/datashare_ext_ability.cpp",
    "${SUBSYSTEM_DIR}/src/datashare_helper.cpp",
    "${SUBSYSTEM_DIR}/src/datashare_proxy.cpp",
    "${SUBSYSTEM_DIR}/src/datashare_stub.cpp",
    "${SUBSYSTEM_DIR}/src/datashare_stub_impl.cpp",
    "${SUBSYSTEM_DIR}/src/extension.cpp",
    "${SUBSYSTEM_DIR}/src/extension_impl.cpp",
    "${SUBSYSTEM_DIR}/src/extension_module_loader.cpp",
    "${SUBSYSTEM_DIR}/src/form_extension.cpp",

    #"${SUBSYSTEM_DIR}/src/dummy_data_ability_predicates.cpp",
    #"${SUBSYSTEM_DIR}/src/dummy_result_set.cpp",
    #"${SUBSYSTEM_DIR}/src/dummy_values_bucket.cpp",
    "${SUBSYSTEM_DIR}/src/form_js_event_handler.cpp",
    "${SUBSYSTEM_DIR}/src/form_provider_client.cpp",
    "${SUBSYSTEM_DIR}/src/form_runtime/form_extension_provider_client.cpp",
    "${SUBSYSTEM_DIR}/src/form_runtime/js_form_extension.cpp",
    "${SUBSYSTEM_DIR}/src/form_runtime/js_form_extension_context.cpp",
    "${SUBSYSTEM_DIR}/src/form_runtime/js_form_extension_util.cpp",
    "${SUBSYSTEM_DIR}/src/js_datashare_ext_ability.cpp",
    "${SUBSYSTEM_DIR}/src/js_datashare_ext_ability_context.cpp",
    "${SUBSYSTEM_DIR}/src/js_service_extension.cpp",
    "${SUBSYSTEM_DIR}/src/js_service_extension_context.cpp",
    "${SUBSYSTEM_DIR}/src/js_static_subscriber_extension.cpp",
    "${SUBSYSTEM_DIR}/src/js_static_subscriber_extension_context.cpp",
    "${SUBSYSTEM_DIR}/src/mission_information.cpp",
    "${SUBSYSTEM_DIR}/src/new_ability_impl.cpp",
    "${SUBSYSTEM_DIR}/src/page_ability_impl.cpp",
    "${SUBSYSTEM_DIR}/src/service_ability_impl.cpp",
    "${SUBSYSTEM_DIR}/src/service_extension.cpp",
    "${SUBSYSTEM_DIR}/src/static_subscriber_extension.cpp",
    "${SUBSYSTEM_DIR}/src/static_subscriber_stub_imp.cpp",
    "//foundation/aafwk/standard/services/abilitymgr/src/ability_start_setting.cpp",
    "//foundation/aafwk/standard/services/abilitymgr/src/launch_param.cpp",

    # "//foundation/aafwk/standard/interfaces/kits/napi/aafwk/featureAbility/feature_ability.cpp",
    # "//foundation/aafwk/standard/interfaces/kits/napi/aafwk/featureAbility/napi_context.cpp",
    # "//foundation/aafwk/standard/interfaces/kits/napi/aafwk/featureAbility/napi_data_ability_helper.cpp",
    "${SUBSYSTEM_DIR}/src/continuation/distributed/continuation_handler.cpp",
    "${SUBSYSTEM_DIR}/src/continuation/distributed/continuation_manager.cpp",
    "${SUBSYSTEM_DIR}/src/continuation/distributed/reverse_continuation_scheduler_primary.cpp",
    "${SUBSYSTEM_DIR}/src/continuation/distributed/reverse_continuation_scheduler_primary_proxy.cpp",
    "${SUBSYSTEM_DIR}/src/continuation/distributed/reverse_continuation_scheduler_primary_stub.cpp",
    "${SUBSYSTEM_DIR}/src/continuation/distributed/reverse_continuation_scheduler_recipient.cpp",
    "${SUBSYSTEM_DIR}/src/continuation/distributed/reverse_continuation_scheduler_replica.cpp",
    "${SUBSYSTEM_DIR}/src/continuation/distributed/reverse_continuation_scheduler_replica_proxy.cpp",
    "${SUBSYSTEM_DIR}/src/continuation/distributed/reverse_continuation_scheduler_replica_stub.cpp",
    "${SUBSYSTEM_DIR}/src/continuation/remote_register_service/connect_callback_proxy.cpp",
    "${SUBSYSTEM_DIR}/src/continuation/remote_register_service/connect_callback_stub.cpp",
    "${SUBSYSTEM_DIR}/src/continuation/remote_register_service/continuation_connector.cpp",
    "${SUBSYSTEM_DIR}/src/continuation/remote_register_service/continuation_device_callback_proxy.cpp",
    "${SUBSYSTEM_DIR}/src/continuation/remote_register_service/continuation_register_manager.cpp",
    "${SUBSYSTEM_DIR}/src/continuation/remote_register_service/continuation_register_manager_proxy.cpp",
    "${SUBSYSTEM_DIR}/src/continuation/remote_register_service/remote_register_service_proxy.cpp",
    "${SUBSYSTEM_DIR}/src/continuation/remote_register_service/remote_register_service_stub.cpp",
    "${SUBSYSTEM_DIR}/src/distributed_ability_runtime/distributed_client.cpp",
    "//foundation/aafwk/standard/frameworks/kits/appkit/native/ability_runtime/context/context_impl.cpp",
    "//foundation/aafwk/standard/frameworks/kits/appkit/native/ability_runtime/datashare_ext_ability_context.cpp",
    "//foundation/aafwk/standard/frameworks/kits/appkit/native/ability_runtime/extension_context.cpp",
    "//foundation/aafwk/standard/frameworks/kits/appkit/native/ability_runtime/form_extension_context.cpp",
    "//foundation/aafwk/standard/frameworks/kits/appkit/native/ability_runtime/service_extension_context.cpp",
    "//foundation/aafwk/standard/frameworks/kits/appkit/native/ability_runtime/static_subscriber_extension_context.cpp",
    "//foundation/aafwk/standard/frameworks/kits/appkit/native/app/src/application_context.cpp",
    "//foundation/aafwk/standard/frameworks/kits/appkit/native/app/src/context_container.cpp",
    "//foundation/aafwk/standard/frameworks/kits/appkit/native/app/src/context_deal.cpp",
    "//foundation/aafwk/standard/frameworks/kits/appkit/native/app/src/sys_mgr_client.cpp",
    "//foundation/aafwk/standard/interfaces/kits/napi/aafwk/inner/napi_common/napi_common_ability.cpp",
    "//foundation/aafwk/standard/interfaces/kits/napi/aafwk/inner/napi_common/napi_common_configuration.cpp",
    "//foundation/aafwk/standard/interfaces/kits/napi/aafwk/inner/napi_common/napi_common_start_options.cpp",
    "//foundation/aafwk/standard/interfaces/kits/napi/aafwk/inner/napi_common/napi_common_util.cpp",
    "//foundation/aafwk/standard/interfaces/kits/napi/aafwk/inner/napi_common/napi_common_want.cpp",
    "//foundation/distributeddatamgr/appdatamgr/frameworks/jskitsimpl/common/src/js_utils.cpp",

    #"//foundation/distributeddatamgr/appdatamgr/frameworks/jskitsimpl/napi_rdb/napi_rdb_predicates.cpp",
    "${SUBSYSTEM_DIR}/src/ability_runtime/js_ability.cpp",
    "${SUBSYSTEM_DIR}/src/ability_runtime/js_ability_context.cpp",
    "${SUBSYSTEM_DIR}/src/ability_runtime/js_caller_complex.cpp",
    "//foundation/distributeddatamgr/appdatamgr/frameworks/jskitsimpl/napi_dataability/src/napi_data_ability_predicates.cpp",
    "//foundation/distributeddatamgr/appdatamgr/frameworks/jskitsimpl/napi_resultset/src/napi_result_set.cpp",
  ]
  configs = [ ":ability_config" ]
  public_configs = [
    ":ability_public_config",
    "//base/notification/ans_standard/frameworks/wantagent:wantagent_innerkits_public_config",
    "//foundation/aafwk/standard/frameworks/kits/ability/ability_runtime:ability_context_public_config",
  ]

  deps = [
    ":static_subscriber_ipc",
    "${INNERKITS_PATH}/base:base",
    "${INNERKITS_PATH}/want:want",
    "//base/notification/ans_standard/frameworks/wantagent:wantagent_innerkits",
    "//base/security/permission/interfaces/innerkits/permission_standard/permissionsdk:libpermissionsdk_standard",
    "//foundation/aafwk/standard/common/task_dispatcher:task_dispatcher",
    "//foundation/aafwk/standard/frameworks/kits/ability/ability_runtime:ability_context_native",
    "//foundation/aafwk/standard/frameworks/kits/appkit:app_context",
    "//foundation/aafwk/standard/interfaces/innerkits/ability_manager:ability_manager",
    "//foundation/aafwk/standard/interfaces/innerkits/app_manager:app_manager",
    "//foundation/aafwk/standard/interfaces/innerkits/dataobs_manager:dataobs_manager",
    "//foundation/ace/napi:ace_napi",
    "//foundation/appexecfwk/standard/common:libappexecfwk_common",
    "//foundation/appexecfwk/standard/interfaces/innerkits/appexecfwk_base:appexecfwk_base",
    "//foundation/appexecfwk/standard/interfaces/innerkits/appexecfwk_core:appexecfwk_core",
    "//foundation/distributedschedule/dmsfwk/interfaces/innerkits/uri:zuri",
    "//foundation/distributedschedule/samgr/interfaces/innerkits/samgr_proxy:samgr_proxy",
    "//foundation/multimodalinput/input/frameworks/proxy:libmmi-common",
    "//foundation/resourceschedule/background_task_mgr/interfaces/innerkits:bgtaskmgr_innerkits",
    "//third_party/icu/icu4c:shared_icuuc",
  ]

  external_deps = [
    "ability_runtime:ability_context_native",
    "ability_runtime:app_manager",
    "ability_runtime:napi_base_context",
    "ability_runtime:runtime",
    "access_token:libaccesstoken_sdk",
    "bytrace_standard:bytrace_core",
    "ces_standard:cesfwk_innerkits",
    "distributeddataobject:distributeddataobject_impl",
    "form_runtime:fmskit_native",
    "form_runtime:form_manager",
    "ipc:ipc_core",
    "ipc_js:rpc",
    "multimodalinput_base:libmmi-client",
    "native_appdatamgr:native_appdatafwk",
    "native_appdatamgr:native_dataability",
    "native_appdatamgr:native_rdb",
    "utils_base:utils",
  ]

  public_deps = [
    "//base/global/i18n_standard/frameworks/intl:intl_util",
    "//base/global/resmgr_standard/frameworks/resmgr:global_resmgr",
    "//base/hiviewdfx/hilog/interfaces/native/innerkits:libhilog",
    "//foundation/ace/napi:ace_napi",
    "//foundation/appexecfwk/standard/interfaces/innerkits/libeventhandler:libeventhandler",
    "//foundation/graphic/standard:libwmclient",
    "//foundation/windowmanager/interfaces/kits/napi/window_runtime:windowstage_kit",
    "//foundation/windowmanager/wm:libwm",
  ]

  subsystem_name = "aafwk"
  part_name = "ability_runtime"
}

ohos_shared_library("dummy_classes") {
  sources = [
    "${SUBSYSTEM_DIR}/src/dummy_data_ability_predicates_discard.cpp",
    "${SUBSYSTEM_DIR}/src/dummy_result_set_discard.cpp",
    "${SUBSYSTEM_DIR}/src/dummy_values_bucket_discard.cpp",
  ]

  configs = [ ":ability_config" ]
  public_configs = [ ":ability_public_config" ]

  deps = [
    "//foundation/appexecfwk/standard/common:libappexecfwk_common",
    "//utils/native/base:utils",
  ]

  external_deps = [
    "hiviewdfx_hilog_native:libhilog",
    "ipc:ipc_core",
  ]

  subsystem_name = "aafwk"
  part_name = "ability_runtime"
}

ohos_shared_library("service_extension_module") {
  include_dirs =
      [ "//foundation/aafwk/standard/frameworks/kits/ability/native/include" ]

  sources = [ "//foundation/aafwk/standard/frameworks/kits/ability/native/src/service_extension_module_loader.cpp" ]

  configs = [ ":ability_config" ]
  public_configs = [ ":ability_public_config" ]

  deps = [
    ":abilitykit_native",
    "//foundation/appexecfwk/standard/common:libappexecfwk_common",
    "//utils/native/base:utils",
  ]

  external_deps = [
    "ability_runtime:runtime",
    "hiviewdfx_hilog_native:libhilog",
  ]

  subsystem_name = "aafwk"
  part_name = "ability_runtime"
}

ohos_shared_library("datashare_ext_ability_module") {
  include_dirs =
      [ "//foundation/aafwk/standard/frameworks/kits/ability/native/include" ]

  sources = [ "//foundation/aafwk/standard/frameworks/kits/ability/native/src/datashare_ext_ability_module_loader.cpp" ]

  configs = [ ":ability_config" ]
  public_configs = [ ":ability_public_config" ]

  deps = [
    ":abilitykit_native",
    "//foundation/appexecfwk/standard/common:libappexecfwk_common",
    "//utils/native/base:utils",
  ]

  external_deps = [
    "ability_runtime:runtime",
    "hiviewdfx_hilog_native:libhilog",
  ]

  subsystem_name = "aafwk"
  part_name = "ability_runtime"
}
